import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login',
    loadChildren: () => import('./features/pages/login/login.module').then( m => m.LoginPageModule)
  },
  { 
    path: 'home', 
   loadChildren: () => import('./features/pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'cafeteria-booking',
    loadChildren: () => import('./features/pages/cafeteria-booking/cafeteria-booking.module').then( m => m.CafeteriaBookingPageModule)
  }
 
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
