import { Component, OnInit } from '@angular/core';
import { CafeteriaBookingService } from '../../services/cafeteria-booking.service';
import { NavController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-cafeteria-booking',
  templateUrl: './cafeteria-booking.page.html',
  styleUrls: ['./cafeteria-booking.page.scss'],
})
export class CafeteriaBookingPage implements OnInit {
  slotsData:any;
  selectedRadioGroup:any;  
  selectedState:any;
  constructor( private cafeteriaBookingService:CafeteriaBookingService,  public alertController: AlertController,  private navController: NavController) { 
    cafeteriaBookingService.fetchBookingSlots().subscribe((result)=>{
      this.slotsData = result;
      console.log("data of slots", this.slotsData);
    });
  }

  ngOnInit() { 
    this.cafeteriaBookingService.fetchBookingSlots().subscribe((result)=>{
      this.slotsData = result;
      console.log("data of slots", this.slotsData);
    });
  }

  ionViewWillEnter(){
    this.cafeteriaBookingService.fetchBookingSlots().subscribe((result)=>{
      this.slotsData = result;
      console.log("ion will enter", this.slotsData);
    });
  }

  slotGroupChange(event){
    console.log("radioGroupChange",event.detail , this.selectedState);
    this.selectedRadioGroup = event.detail;
  }

  slotSelect(event){
   console.log("selected data", event.detail);   
  }

 async bookSlotClick(){
    let index =  this.slotsData.cafeteria_slots.findIndex(data => data.slot_id === this.selectedState);
    console.log("filtered item", index );
    this.slotsData.cafeteria_slots[index].is_booked = true;
    console.log("updated item",  this.slotsData.cafeteria_slots);
    localStorage.setItem("bookSlotData",JSON.stringify(this.slotsData.cafeteria_slots));
    let alert = await this.alertController.create( {
      header : "Your slot is booked !",           
      buttons : [
         {
              text: 'OK',
              handler: langauge => {                 
                  this.navController.navigateForward('/home');      }
        }
      ]
  } );
  await alert.present();
   
  }

}
