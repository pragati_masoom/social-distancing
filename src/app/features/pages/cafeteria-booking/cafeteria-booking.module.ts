import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CafeteriaBookingPage } from './page/cafeteria-booking/cafeteria-booking.page';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/global/shared.module';
import { CafeteriaBookingService } from './services/cafeteria-booking.service';



const routes: Routes = [
  {
    path: '',
    component: CafeteriaBookingPage
  }
];


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild(routes) 
  ],
  declarations: [CafeteriaBookingPage],
  providers:[CafeteriaBookingService]

})
export class CafeteriaBookingPageModule {}
