import { TestBed } from '@angular/core/testing';

import { CafeteriaBookingService } from './cafeteria-booking.service';

describe('CafeteriaBookingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CafeteriaBookingService = TestBed.get(CafeteriaBookingService);
    expect(service).toBeTruthy();
  });
});
