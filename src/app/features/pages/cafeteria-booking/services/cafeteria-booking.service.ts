import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {of} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class CafeteriaBookingService {
data:any= {"cafeteria_slots":[    
  {"slot_id":"1001", "start_time":"12:00 PM", "end_time":"12:20 PM", "is_booked":false, checked: true,},    
  {"slot_id":"1002", "start_time":"12:30 PM", "end_time":"12:50 PM", "is_booked":false, checked: false,},  
  {"slot_id":"1003", "start_time":"01:00 PM", "end_time":"01:20 PM", "is_booked":false, checked: false,},    
  {"slot_id":"1004", "start_time":"01:30 PM", "end_time":"01:50 PM", "is_booked":false, checked: false,}   
]}  

  constructor() { }

  fetchBookingSlots():Observable<any[]> {
    let slotData = of(this.data);
    return slotData;
  }
}
