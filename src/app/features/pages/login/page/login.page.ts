import { Component, OnInit } from '@angular/core';
import { SideBarService } from 'src/app/global/services/side-bar.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(public sideBarService:SideBarService, private navController: NavController) { 
  
  }

  async ionViewWillEnter() {
    this.sideBarService.enableSidebar();    
    this.sideBarService.showLogout = true;
  }
  
  ngOnInit() {
  }

  /*
  * Redirect to home page 
  */
 loginClick(){
    this.navController.navigateForward('/home');    
  }

}
