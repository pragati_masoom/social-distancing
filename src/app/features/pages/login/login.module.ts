import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { LoginPage } from './page/login.page';
import { RouterModule } from '@angular/router';
import { SharedModule, createTranslateLoader } from 'src/app/global/shared.module';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    TranslateModule.forChild({loader: {
      provide: TranslateLoader,
      useFactory:(createTranslateLoader),
      deps: [HttpClient]
    }}),
    RouterModule.forChild([
      {
        path: '',
        component: LoginPage
      }
    ])
  
  ],
  declarations: [LoginPage]
})
export class LoginPageModule {}
