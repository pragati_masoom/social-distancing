import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { SideBarService } from 'src/app/global/services/side-bar.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  bookSlotData:any;
  constructor(
    public sideBarService: SideBarService,
    private navController: NavController
    ) {
      if(localStorage.getItem("bookSlotData"))
        this.bookSlotData = JSON.parse(localStorage.getItem("bookSlotData"));
    }

    async ionViewWillEnter() {
      this.sideBarService.enableSidebar();    
      this.sideBarService.showLogout = true;
    }
    
    bookingCafeteriaClick(){      
      this.navController.navigateRoot('/cafeteria-booking');       
    }
 
}
