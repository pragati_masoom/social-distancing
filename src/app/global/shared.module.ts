import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { BackButtonComponent } from './components/back-button/back-button.component';
import { MenuButtonComponent } from './components/menu-button/menu-button.component';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [ BackButtonComponent, MenuButtonComponent],
  imports: [
    CommonModule,
    IonicModule,
    TranslateModule.forChild({loader: {
      provide: TranslateLoader,
      useFactory:(createTranslateLoader),
      deps: [HttpClient]
    }})
  ],
  providers : [   
  ],
  exports : [  BackButtonComponent, MenuButtonComponent]
})
export class SharedModule { }