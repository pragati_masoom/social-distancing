/**
 * @author [Mohit Arora]
 * @email [mohit.arora@soprasteria.com]
 * @create date 2020-30-12 10:48:27
 * @modify date 2020-30-12 10:48:27
 * @desc [description]
 */

import { GlobalPopupModalService } from './global-popup/services/global-popup-modal.service';

export abstract class KpiModalBase {

  constructor(protected modalService: GlobalPopupModalService) { }

  close(result?) {
    this.modalService.closeActiveModal(this, result);
  }


  dismiss(reason?) {
    this.modalService.dismissActiveModal(this, reason);
  }
}
