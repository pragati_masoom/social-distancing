import { Injectable } from '@angular/core';
import { NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgbModal, NgbModalOptions, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { KpiModalOptions, KpiModalRef, activeModals } from 'src/app/global/models/kpi-modal-model';

@Injectable({
  providedIn: 'root'
})
export class GlobalPopupModalService {

  constructor(private modalService: NgbModal) { }

  open(content: any, options?: KpiModalOptions): KpiModalRef {
    options = options || {} as KpiModalOptions;

    const modalRef: NgbModalRef = this.modalService.open(content, {
      centered: true,
      backdrop: 'static',
      keyboard: false,
      windowClass: 'bg-dark-grey',
      ...options,
    } as NgbModalOptions);

    activeModals.push(modalRef);

    return new KpiModalRef(modalRef);
  }

  closeActiveModal(modalComp, closeString?): void {
    const index = this.getModalIndex(modalComp);
    activeModals.splice(index, 1)[0].close(closeString);
  }

  dismissActiveModal(modalComp, reason?): void {
    const index = this.getModalIndex(modalComp);
    activeModals.splice(index, 1)[0].dismiss(reason);
  }

  closeAll(): void {
    activeModals.forEach(modalRef => {
      modalRef.close();
    });
  }

  private getModalIndex(modalComp): number {
    return activeModals.findIndex(modalRef => {
      try {
        return modalRef.componentInstance === modalComp;
      } catch (e) {
        return false;
      }
    });
  }

}

