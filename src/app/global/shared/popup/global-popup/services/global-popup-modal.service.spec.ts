import { TestBed } from '@angular/core/testing';

import { GlobalPopupModalService } from './global-popup-modal.service';

describe('GlobalPopupModalService', () => {
  let service: GlobalPopupModalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(GlobalPopupModalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
