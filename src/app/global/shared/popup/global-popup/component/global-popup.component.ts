import { Component } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { GlobalPopupModalService } from '../services/global-popup-modal.service';
import { KpiDialogConfig, DialogActionList } from 'src/app/global/models/kpi-modal-events';
import { CopyConfirmationPopupComponent } from '../../copy-confirmation-popup/component/copy-confirmation-popup.component';
import { KpiModalBase } from '../../kpi-modal-base';


@Component({
  selector: 'app-global-popup',
  templateUrl: './global-popup.component.html',
  styleUrls: ['./global-popup.component.scss']
})
export class GlobalPopupComponent extends KpiModalBase {
  kpiPopup: KpiDialogConfig = {} as KpiDialogConfig;

  constructor(protected modalService: GlobalPopupModalService, private translate: TranslateService) {
    super(modalService);
  }


  dialogCloseHandler(action: any) {
    if (action.buttonLabel === DialogActionList.COPY.buttonLabel) {
      /**
        * This is an exportable function to copy any set of data in string format.
        */

    //  copyTextAreaToClipBoard(this.kpiPopup.message);

      const popup = this.modalService.open(CopyConfirmationPopupComponent, {
        windowClass: 'bg-dark-grey',
        size: <any>'md'
      });
      /**
        * @description THis is popup generation of confirmation that the sting is copied when copy button is clicked.
        */
       popup.componentInstance.copyPopup = {
        title: this.translate.instant('popup.error.clipboard.title'),
        message: this.translate.instant('popup.error.clipboard.message'),
        actionList: [DialogActionList.CLOSE],
      };
    } else {
      this.close({ action: { action }, data: '' });
    }
  }
}
