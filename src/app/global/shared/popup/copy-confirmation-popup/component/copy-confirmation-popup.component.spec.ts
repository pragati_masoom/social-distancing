import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyConfirmationPopupComponent } from './copy-confirmation-popup.component';

describe('CopyConfirmationPopupComponent', () => {
  let component: CopyConfirmationPopupComponent;
  let fixture: ComponentFixture<CopyConfirmationPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CopyConfirmationPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyConfirmationPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
