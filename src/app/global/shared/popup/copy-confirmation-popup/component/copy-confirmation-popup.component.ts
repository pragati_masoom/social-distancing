import { Component, OnInit } from '@angular/core';
import { KpiDialogConfig } from 'src/app/global/models/kpi-modal-events';
import { GlobalPopupModalService } from '../../global-popup/services/global-popup-modal.service';
import { KpiModalBase } from '../../kpi-modal-base';

@Component({
  selector: 'app-copy-confirmation-popup',
  templateUrl: './copy-confirmation-popup.component.html',
  styleUrls: ['./copy-confirmation-popup.component.scss']
})
export class CopyConfirmationPopupComponent  extends KpiModalBase {
  copyPopup: KpiDialogConfig

  constructor(protected modalService: GlobalPopupModalService) {
    super(modalService);
  }

  /**
   * @description This method is used to close the popup.
   * In case if the action is nedeed in future, action parameter can be used to manipulate data.
   *
   * @param {*} action
   * @memberof CopyConfirmationPopupComponent
   */
  dialogCloseHandler(action: any) {
    this.close({ action: action, data: '' });
  }
}
