export interface DialogCloseAction {
    action: any;
    data?: any;
  }
  
  export interface DialogAction {
    buttonLabel: string;
    iconClass: string;
  }
  
  /**
    * The Constant value should match with the translation key of respective action labels
    **/
  
  // tslint:disable-next-line: no-namespace
  export namespace DialogActionList {
    export const OK: DialogAction = { buttonLabel: 'action.ok', iconClass: 'fas fa-check' };
    export const YES: DialogAction = { buttonLabel: 'action.yes', iconClass: 'fas fa-check' };
    export const CLOSE: DialogAction = { buttonLabel: 'action.close', iconClass: 'fas fa-window-close' };
    export const PROCEED: DialogAction = { buttonLabel: 'button.forceSubmit.label', iconClass: 'fas fa-arrow-right' };
    export const NO: DialogAction = { buttonLabel: 'action.no', iconClass: 'fas fa-window-close' };
    export const CANCEL: DialogAction = { buttonLabel: 'action.cancel', iconClass: 'fas fa-window-close' };
    export const COPY: DialogAction = { buttonLabel: 'action.copy', iconClass: 'far fa-copy' };
    export const SEND: DialogAction = { buttonLabel: 'action.send', iconClass: 'fas fa-paper-plane' };
    export const SKIP: DialogAction = { buttonLabel: 'action.skip', iconClass: 'fas fa-fast-forward' };
    export const UPLOAD: DialogAction = { buttonLabel: 'action.upload', iconClass: 'fas fa-upload' };
  }
  
  export interface KpiDialogConfig {
    title: string;
    i18nLabel: string;
    message: string;
    actionList: any[];
    parameters?: any;
    onlyWarning?: boolean;
    fetchFromCIB?: boolean;
    isCIB?: boolean;
    isTransition?: boolean;
    closeResult?: string;
    question?: string;
    errorFileName?: string;
    popUpType?: string;
    method?: string;
    displayCurrencyWarning?: boolean;
    logFilePath?: string;
    dataFilePath?: string;
    isCloseVisible?: boolean;
    copyMessage?: string;
  }
  