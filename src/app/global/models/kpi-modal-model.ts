/**
 * @author [Mohit Arora]
 * @email [mohit.arora@soprasteria.com]
 * @create date 2020-03-30 10:50:02
 * @modify date 2020-03-30 10:50:02
 * @desc [description]
 */
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';

/**
 * List to hold all active modal instances
 */
export const activeModals: NgbModalRef[] = [];

export class KpiModalRef {
  result: Promise<any>;

  private compInstance?: any;
  private modalRef: NgbModalRef;

  get componentInstance(): any {
    return this.compInstance;
  }

  set componentInstance(comp: any) {
    this.compInstance = comp;
  }

  constructor(modalRef: NgbModalRef) {
    this.modalRef = modalRef;
    this.result = modalRef.result;
    this.componentInstance = modalRef.componentInstance;
  }

  close(value?) {
    const modalIndex = activeModals.indexOf(this.modalRef);
    activeModals.splice(modalIndex, 1)[0].close(value);
  }

  dismiss(reason?) {
    const modalIndex = activeModals.indexOf(this.modalRef);
    activeModals.splice(modalIndex, 1)[0].dismiss(reason);
  }

  setProps(props: Object) {
    if (props) {
      for (let prop in props) {
        if (props.hasOwnProperty(prop)) {
          this.componentInstance[prop] = props[prop];
        }
      }
    }
  }
}

/**
 * Interface to be used for option with open method of KpiModalService
 */
export interface KpiModalOptions {
  size?: KpiModalSizeType;
  windowClass?: string;
  centered?: boolean;
  backdrop?: string;
  keyboard?: boolean;
}

/**
 * Type with fixed value for modal size
 */
type KpiModalSizeType = 'sm' | 'md' | 'lg' | 'xl' | 'xxl';

/**
 * Constants for modal size
 */
// tslint:disable-next-line:no-namespace
export namespace KpiModalSize {
  export const SM: KpiModalSizeType = 'sm';
  export const MD: KpiModalSizeType = 'md';
  export const LG: KpiModalSizeType = 'lg';
  export const XL: KpiModalSizeType = 'xl';
  export const FILL: KpiModalSizeType = 'xxl';
}

