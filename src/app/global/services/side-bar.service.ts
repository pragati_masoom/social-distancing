import { Injectable } from "@angular/core";
import { MenuController, NavController, AlertController } from '@ionic/angular';
import { TranslateOptionService } from './translate.service';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { LocalStorageConstants } from '../constants/local-storage.constants';
import { Dialogs } from '@ionic-native/dialogs/ngx';

@Injectable({
    providedIn : 'root'
})
export class SideBarService{

    private menuId : string = "first"
    public updateUserInfo = new Subject<string>()

    public showLogout : boolean = false; 

    public username : string = '';
    public userrole : string = '';

    constructor( private menu: MenuController,
        private dialogs: Dialogs,
        private translateOptionService : TranslateOptionService,     
        private navCtrl : NavController,
        public alertController: AlertController,
        private translate : TranslateService  ) {
        this.updateUserInfo.subscribe( () => {
            this.username = localStorage.getItem(LocalStorageConstants.USER_NAME)           
        } )
    }

    enableSidebar () {
        this.menu.enable(true, this.menuId);        
    }

    disableSidebar () {
        this.menu.enable(false, this.menuId);
    }

    async openSidebar () {
        await this.menu.open(this.menuId);
    }

    async closeSidebar () {
        await this.menu.close(this.menuId);
    }

    async languageSwitchOption () {
        this.closeSidebar();
        this.translateOptionService.showAlert()  
    }

    async switchThemeOption() {
        this.closeSidebar();        
    }    

    async logoutOption () {
        this.closeSidebar();
        // this.dialogs.confirm( this.translate.instant('LOGOUT_CONFIRMATION') , "Sopra Steria").then( ( result ) => {
        //     if(result == 1) {               
        //         localStorage.clear();   
        //         this.navCtrl.navigateBack('/login');
        //     }
        // })
        let alert = await this.alertController.create( {
            header : "Do you really want to logout?",           
            buttons : [
                {
                    text : "Cancel"
                },
                {
                    text: 'OK',
                    handler: langauge => {
                        localStorage.clear();     
                        this.navCtrl.navigateBack('/login');      }
              }
            ]
        } );
        await alert.present();
    }

}