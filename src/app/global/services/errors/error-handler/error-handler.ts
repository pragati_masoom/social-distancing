import { DatePipe } from '@angular/common';
import { HttpErrorResponse } from '@angular/common/http';
import { ErrorHandler, Injectable, Injector, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ErrorsService } from '../error-service/error.service';
import { DialogActionList } from 'src/app/global/models/kpi-modal-events';
import { BasePopupService } from '../../base-popup.service';

const authErrorCode = 401;
const errorCodeList = [400, 403, 404, 405, 401];
const functionalErrorList = [422, 1001, 1004];
const technicalErrorList = [500, 1006];

@Injectable()
export class ErrorsHandler implements ErrorHandler {

  private unHandledErrorcaught: boolean;
  private messageUtilsService: BasePopupService;

  // flag to prevent repeated handling for same error type in case of API server down or Authentication error
  private blockMoreHandling: boolean;

  constructor(
    private injector: Injector,
    private translate: TranslateService,
    private ngZone: NgZone,
    private datePipe: DatePipe) {
  }

  handleError(error: any): void {
    if (this.blockMoreHandling) {
      return;
    }
    const errorsService = this.injector.get<ErrorsService>(ErrorsService);
    this.messageUtilsService = this.injector.get<BasePopupService>(BasePopupService);

    const errorWithContext = errorsService.addContextInfo(error);
    let errorObject = {};
    if (error.rejection instanceof HttpErrorResponse || error instanceof HttpErrorResponse) {
      // in case of response type is text instead json object
      if (error.error && typeof error.error === 'string') {
        try {
          error.error = JSON.parse(error.error);
        } catch (err) {
          this.ngZone.run(() => {
            console.log(error);
            this.messageUtilsService.displayAbnormalError();
          });
        }
      } else {
        const httpError = (error.rejection && error.rejection.error) || error.error;
        if (error.status === 0 || (error.rejection && error.rejection.status === 0)) {
          this.blockMoreHandling = true;
          this.showNoInternetError();
        } else {
          this.showErrorBasedOnStatusCode(httpError, errorWithContext, errorObject);
        }
      }
    } else {
      if (!navigator.onLine) {
        this.showNoInternetError();
      } else if (!this.unHandledErrorcaught && !error.message.includes('ExpressionChangedAfterItHasBeenCheckedError')) {
        this.unHandledErrorcaught = true;
        this.ngZone.run(() => {
          console.log(error);
          this.messageUtilsService.displayAbnormalError().subscribe(() => {
            this.unHandledErrorcaught = false;
          });
        });
      }
    }
  }

  /**
   * This method will display message when there is no internet or server down
   *
   * @private
   * @memberof ErrorsHandler
   */
  private showNoInternetError() {
    this.ngZone.run(() => {
      this.messageUtilsService.displayError(this.translate.instant('popup.error.global.title'),
        this.translate.instant('popup.error.no.internet')).subscribe(() => {
          this.blockMoreHandling = false;
        });
    });
  }

  private showErrorBasedOnStatusCode(httpError, errorWithContext, errorObject) {
    let message: string;
    const date = this.datePipe.transform(new Date(), 'dd-MMM-y HH:mm zzzz');
    /*
      Based on the status of the http errors ned to handle the errors.
    */
    if (errorCodeList.includes(httpError.code)) {
      message = httpError.status + '-' + this.translate.instant('popup.error.global.text', [date]);
      if (httpError.code === authErrorCode) {
        message = httpError.code + '-' + httpError.message;
        this.blockMoreHandling = true;
        this.ngZone.run(() => {
          this.messageUtilsService.displayError(this.translate.instant('error.session.newUserLoggedInTitle'), message,
            [DialogActionList.OK], true).subscribe(() => {
              this.blockMoreHandling = false;
              this.injector.get(Router).navigate(['logout'], { skipLocationChange: true });
            });
        });
        return;
      }
    } else {
      message = httpError.message;
    }
    if (httpError.title) {
      errorObject.title = this.translate.instant(httpError.title);
    } else if (functionalErrorList.includes(httpError.code)) {
      errorObject.title = this.translate.instant('popup.error.functional.title');
    } else if (technicalErrorList.includes(httpError.code)) {
      errorObject.title = this.translate.instant('popup.error.technical.title');
    } else {
      errorObject.title = this.translate.instant('popup.error.global.title');
    }
    const errorStatus = httpError.status.toString() + '-' + httpError.error;
    // const errorTitle = httpError.status.toString();
    errorObject.message = errorStatus.bold() + '<br>' + message ||
    (httpError.code + '-' + this.translate.instant('popup.error.global.text', [date]));
    errorObject.complement = httpError.complement || '';

    this.ngZone.run(() => {
      this.messageUtilsService.displayError(
        errorObject.title,
        errorObject.message,
        [DialogActionList.COPY, DialogActionList.CANCEL],
        false,
        errorObject.complement
      );
    });
  }
}
