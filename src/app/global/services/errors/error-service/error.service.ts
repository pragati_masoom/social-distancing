import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import * as StackTraceParser from 'error-stack-parser';

@Injectable()
export class ErrorsService {

  constructor() { }
  addContextInfo(error) {
    const status = error.status || null;
    const message = error.message || error.toString();
    const stack = error instanceof HttpErrorResponse ? null : StackTraceParser.parse(error);

    const errorWithContext = {
      status,
      message,
      stack
    };

    return errorWithContext;
  }

}

export function getAlertTitle(translate, httpError): string {
  let alertTitle = '';
  if (httpError.errorCode === 1004 || httpError.errorCode === 1001) {
    alertTitle = translate.instant('popup.error.functional.title');
  } else if (httpError.errorCode === 1006) {
    alertTitle = translate.instant('popup.error.technical.title');
  } else {
    alertTitle = translate.instant('popup.error.global.title');
  }

  return alertTitle;
}
