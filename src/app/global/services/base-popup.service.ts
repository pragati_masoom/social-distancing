import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DatePipe } from '@angular/common';
import { DialogAction, DialogActionList, DialogCloseAction } from '../models/kpi-modal-events';
import { KpiModalRef } from '../models/kpi-modal-model';
import { Observable , Subject} from 'rxjs';
import { GlobalPopupModalService } from '../shared/popup/global-popup/services/global-popup-modal.service';
import { GlobalPopupComponent } from '../shared/popup/global-popup/component/global-popup.component';

@Injectable({
  providedIn: 'root'
})
export class BasePopupService {

  constructor(protected modalService: GlobalPopupModalService, private translate: TranslateService, private datePipe: DatePipe) { }


  basePopup(title: string, message: string, actionLists: DialogAction[], parameters?: any, i18nLabel?: boolean,
            onlyWarning?: boolean, copyMessage?: string): KpiModalRef {
    const iconPopup = (onlyWarning) ? 'fas fa-3x fa-exclamation-triangle text-primary' :
      'fas fa-3x fa-exclamation-triangle text-danger text-center';
    const popup = this.modalService.open(GlobalPopupComponent, {
      keyboard: false,
      backdrop: 'static',
      centered: true,
      size: actionLists.length > 3 ? 'lg' : 'md' as any,
      windowClass: 'bg-dark-grey'
    });
    popup.componentInstance.kpiPopup = {
      title: this.i18translate(title, i18nLabel),
      message: this.getMessage(message, i18nLabel, parameters),
      parameters: {parameters},
      i18nLabel: {i18nLabel},
      actionList: actionLists,
      popUpType: iconPopup,
      copyMessage: {copyMessage}
    };
    return popup;
  }

  displayError(title: string, message: string, actionList: DialogAction[]= [DialogActionList.COPY, DialogActionList.CANCEL],
               onlyWarning: boolean = false, parameters?: any,
               i18nLabel: boolean = true): Observable<DialogCloseAction> {
    const closeAction = new Subject<DialogCloseAction>();
    const popup = this.basePopup(title, message, actionList, parameters, i18nLabel, onlyWarning);
    popup.result.then((content) => {
      closeAction.next(content);
      closeAction.complete();
    });
    return closeAction;
  }
  displayAbnormalError(): Observable<DialogCloseAction> {
    const closeAction = new Subject<DialogCloseAction>();
    const currentDate = this.datePipe.transform(Date.now(), 'dd-MM-yyyy H:mm zzzz');
    const popup = this.basePopup('popup.error.global.title',
     'popup.error.global.text', [DialogActionList.OK], [currentDate], true);
    popup.result.then((content) => {
      closeAction.next(content);
      closeAction.complete();
    });
    return closeAction;
  }

  i18translate(i18Key: string, i18nLabel: boolean): string {
    if (i18nLabel) {
      return this.translate.instant(i18Key);
    } else {
      return i18Key;
    }
  }

  getMessage(message: any, i18nLabel: boolean, parameters: any) {
    let params: any[];
    if (parameters) {
      if (parameters.constructor === Array) {
        params = parameters;
      } else {
        params = [parameters];
      }
    }
    if (i18nLabel) {
      return this.translate.instant(message, params);
    } else {
      return message;
    }
  }
}