import { Injectable } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { AlertController } from '@ionic/angular';
import { LocalStorageConstants } from '../constants/local-storage.constants';


@Injectable()
export class TranslateOptionService {

    translateButton: any

    constructor(public translate: TranslateService, public alertController: AlertController) {

        var lang = localStorage.getItem(LocalStorageConstants.APP_LANGUAGE);
        if( !lang ){
            localStorage.setItem(LocalStorageConstants.APP_LANGUAGE,'en');
            translate.setDefaultLang( 'en' );
        } else {
            translate.setDefaultLang( lang );
        }

    }

    /**
     * Method to display a dialog to manage the application languague
     */
    async showAlert() {        
        var currentLangauge = localStorage.getItem(LocalStorageConstants.APP_LANGUAGE);
        
        // console.log( "Current Language - " , currentLangauge   );

        let alert = await this.alertController.create( {
            header : this.translate.instant('CHOOSE_LANGUANGE'),
            inputs : [
                {
                    type: 'radio',
                    label: 'English',
                    value: 'en',
                    checked: currentLangauge === 'en'
                },
                {
                    type: 'radio',
                    label: 'German',
                    value: 'de',
                    checked: currentLangauge === 'de'
                }        
            ],
            buttons : [
                {
                    text : "Cancel"
                },
                {
                    text: 'OK',
                    handler: langauge => {
                        this.translate.use( langauge );
                        localStorage.setItem(LocalStorageConstants.APP_LANGUAGE,langauge);
                    }
                }
            ]
        } );
       
        await alert.present();

    }
}
