export enum LocalStorageConstants{
  USER_TYPE = "userType",
  USER_NAME = "userName",  
  APP_THEME = "theme",
  APP_LANGUAGE = "lang", 
  APP_ID = "appId", 
}