import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SideBarService } from './global/services/side-bar.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public sideBarService: SideBarService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.sideBarService.updateUserInfo.subscribe( () => {
      //  this.username = localStorage.getItem(LocalStorageConstants.USER_NAME)
      //  this.userrole = localStorage.getItem(LocalStorageConstants.ROLE)
      } );
    });
  }
}
